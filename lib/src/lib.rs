use std::{fmt, str::FromStr};

use crc::{Crc, CRC_32_JAMCRC};

/// Calculates the master password from the system disabled code
///
/// Input must be in the format `203c-d001-xxxx-xxxx-xxxx-xxxx`
pub fn generate_master_password(system_disabled_code: &str) -> Result<String, InvalidKey> {
    let k: RecoveryKey = system_disabled_code.parse()?;

    Ok(k.calculate_password())
}

#[derive(Clone, Copy)]
struct RecoveryKey(u64);

impl RecoveryKey {
    pub fn calculate_password(&self) -> String {
        let crc = Crc::<u32>::new(&CRC_32_JAMCRC);

        let s = format!("{:016x}", self.0);
        let res = crc.checksum(s.as_bytes());
        format!("{res:x}")
    }
}

impl FromStr for RecoveryKey {
    type Err = InvalidKey;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let s = s.to_ascii_lowercase();
        let mut split = s.split('-');

        if split.next() != Some("203c") {
            return Err(InvalidKey);
        }

        if split.next() != Some("d001") {
            return Err(InvalidKey);
        }

        let mut res = 0u64;

        for i in (0..4).rev() {
            let num = split
                .next()
                .and_then(|s| u16::from_str_radix(s, 16).ok())
                .ok_or(InvalidKey)?;
            res |= (num as u64) << (i * 16);
        }

        Ok(RecoveryKey(res))
    }
}

#[derive(Debug)]
/// The entered system disabled code was invalid
///
/// This could be for a few reasons:
/// - Key must consist of six groups of hexadecimal characters
/// - First group must be `203c`
/// - Second group must be `d001`
pub struct InvalidKey;

impl std::error::Error for InvalidKey {}

impl fmt::Display for InvalidKey {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "invalid key")
    }
}

#[cfg(test)]
mod test {
    use std::str::FromStr;

    use super::RecoveryKey;

    #[test]
    fn short_key() {
        let key = RecoveryKey::from_str("203c-d001-0000-001d-e960-227d").unwrap();
        assert_eq!("494eab7c", key.calculate_password())
    }

    #[test]
    fn known_key() {
        let key = RecoveryKey::from_str("203c-d001-4f30-609d-5125-646a").unwrap();
        assert_eq!("66b14918", key.calculate_password())
    }
}
