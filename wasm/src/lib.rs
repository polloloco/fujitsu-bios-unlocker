use wasm_bindgen::prelude::*;

#[wasm_bindgen(start)]
fn main() -> Result<(), JsValue> {
    Ok(())
}

#[wasm_bindgen]
/// Calculates the master password from the system disabled code
///
/// Input must be in the format `203c-d001-xxxx-xxxx-xxxx-xxxx`
pub fn generate_master_password(system_disabled_code: &str) -> Result<String, JsValue> {
    fujitsu_bios_password_lib::generate_master_password(system_disabled_code)
        .map_err(|e| e.to_string().into())
}
