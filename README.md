# What this is

This repository contains a small program to unlock the BIOS of some Fujitsu PCs and Laptops.

# Why?

A few months ago a friend of me bought a used Fujitsu Esprimo D538 E94+ but the BIOS of that PC was locked with a password that we didn't know.

A quick internet search revealed [some](https://bios-fix.com/index.php?resources/unlock-fujitsu-siemens-bios-password-new-models.213/) [websites](https://biospassword.eu/fujitsu) that offer a (paid) service to unlock your BIOS, but they charge up to 30€ for a single BIOS password. Thats ridiculous.

So I decided to reverse engineer the relevant UEFI modules to find out how the password is generated.

# How to use

## [Click here for the easy web version](https://polloloco.gitlab.io/fujitsu-bios-unlocker/)

## Compiling and running the binary locally

You must have [rust](https://www.rust-lang.org/tools/install) installed on your system.

Clone this repository, and then run this command to get your bios password:

```
cargo run -- 203c-d001-xxxx-xxxx-xxxx-xxxx
```

Replace the placeholder with your recovery key that you get from the "System disabled" screen after entering the three [service passwords](#service-passwords).

![system_disabled.png](images/system_disabled.png)

# Reverse engineering

The code that is responsible for generating and displaying the recovery code can be found in the "FjPasswordInvalidHandler" efi module, lets go over the pseudocode produced by our favorite disassembler:

![decompiler_1.png](images/decompiler_1.png)

The first few lines already tell us how the system recovery key is generated: Its just the result of the `__rdtsc` instruction which returns the number of cpu clockcycles since the last system reset. The four words (in this case a word is 16 bits) of the of that 64 bit performance counter are written into the `string_buffer` string and padded with zeros if necessary so that we end up with a string like `203c-d001-xxxx-xxxx-xxxx-xxxx`.

After that follows a for loop that iterates over every character of the string, converting every uppercase character to lowercase.

While the first `snprintf` call isn't used (for whatever reason), the next one is, as we will see soon. The `performance_counter` gets converted to a string and left-padded with up to 16 zeros. It is followed with the same loop as before, to convert the string to lowercase.

![decompiler_2.png](images/decompiler_2.png)

This snippet starts with an inlined `strlen` to get the length of the `buf` string that contains the hex representation of the tsc. Next, the code calculates the crc32 hash of that string and writes it to the `crc` variable, optionally initializing the crc lookup table first. The specific crc algorithm used is CRC-32/JAMCRC.

![decompiler_3.png](images/decompiler_3.png)

Finally, the crc hash gets hex formatted into a string and left-padded with up to 8 zeros. That resulting string is the new password for your PC.

To update your BIOS password, the code first reads the AMITSESetup struct to the `setup` variable. For my BIOS version, the struct looks like this:

```cpp
struct AMITSESetup
{
  CHAR16 user_password[0x20];
  CHAR16 supervisor_password[0x20];
  bool quiet_boot;
};
```

`memcpy` is used to first write the hex-crc string into the `supervisor_password` field, and afterwards the sha256 hash is calculated, and assuming the calculation was successful (why wouldn't it?), it gets saved to the `supervisor_password` field overwriting the cleartext password.

Finally the `AMITSESetup` uefi variable gets saved. After the next system reset, you have to enter the new generated password to unlock the system.

# tldr

After entering the three [service passwords](#service-passwords), the system takes the tsc and sets the new system password to the crc32 hash of that performance counter.

# Service Passwords

In order to see the recovery key in the form of `203c-d001-xxxx-xxxx-xxxx-xxxx` you have to enter three specific passwords in the right order. For my system they are the following:

- `23fbb82a`
- `d2f65c29`
- `ca3db92a`

Depending on your model and bios version, those might not work for you, try using these passwords instead:

- `3hqgo3`
- `jqw534`
- `0qww294e`