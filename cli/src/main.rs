use std::process::exit;

use fujitsu_bios_password_lib::generate_master_password;

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let Some(recovery_key) = std::env::args().nth(1) else {
        eprintln!("Usage: <tool> <recovery_key>");
        eprintln!("Recovery key must be in this format: 203c-d001-xxxx-xxxx-xxxx-xxxx");
        exit(1)
    };

    let master_password = generate_master_password(&recovery_key)?;

    println!("Password: {}", master_password);

    Ok(())
}
